# Maintainer: Mark Wagie <mark at manjaro dot org>
# Contributor: Bernhard Landauer <bernhard[at]manjaro[dot]org>
# Contributor: Helmut Stult
# Contributor: Alexander Epaneshnikov <alex19ep@archlinux.org>
# Contributor: Sam Burgos <santiago.burgos1089@gmail.com>

pkgname=lightdm-slick-greeter
_pkgname=${pkgname#lightdm-}
pkgver=2.0.9
pkgrel=1
pkgdesc='A slick-looking LightDM greeter'
arch=('x86_64')
url="https://github.com/linuxmint/slick-greeter"
license=('GPL3')
depends=('cairo' 'freetype2' 'gtk3' 'libcanberra' 'libxext' 'lightdm' 'pixman'
         'python-gobject' 'xapp' 'xorg-server')
makedepends=('intltool' 'vala' 'gnome-common')
optdepends=('numlockx: enable numerical keypad on supported keyboard')
backup=("etc/lightdm/${_pkgname}.conf")
install="${_pkgname}.install"
source=("$pkgname-$pkgver.tar.gz::${url}/archive/${pkgver}.tar.gz"
        "${_pkgname}.conf"
        "${_pkgname}.png")
sha256sums=('fa0146862ac0967a1a333f9b553d60a5c625c99b903d01aefe9b87bfdb111c29'
            'a015a40fcd2ba09d3744aff7126041d3f2ce2ae6e2939f07b45cd7ae0a482894'
            '1295fc79a111af0834ab95ea2a3a9d2684c42a062b21d88a1a0c88da9ed5ca16')

prepare() {
    cd "${_pkgname}-${pkgver}"
    NOCONFIGURE=1 ./autogen.sh
}

build() {
    cd "${_pkgname}-${pkgver}"
    ./configure \
        --prefix=/usr \
        --sysconfdir=/etc \
        --sbindir=/usr/bin \
        --libexecdir=/usr/lib/lightdm
    make
}

package() {
    cd "${_pkgname}-${pkgver}"
    make DESTDIR="${pkgdir}" install

    # adjust launcher name
    mv "${pkgdir}/usr/share/xgreeters/${_pkgname}.desktop" \
        "${pkgdir}/usr/share/xgreeters/$pkgname.desktop"

    # Install default conf
    install -Dm644 "${srcdir}/${_pkgname}.conf" -t "${pkgdir}/etc/lightdm/"
    install -Dm644 "${srcdir}/${_pkgname}.png" -t "${pkgdir}/usr/share/${_pkgname}/"
}
